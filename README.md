Build

```bash
mvn clean package
```

Deploy

```bash
cf push --hostname pcf-demo
```

Check Logs

```bash
cf logs pcf-demo --recent  
```