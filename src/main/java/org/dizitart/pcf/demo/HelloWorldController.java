package org.dizitart.pcf.demo;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.AbstractController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class HelloWorldController extends AbstractController {
    private Log log = LogFactory.getLog(getClass());

    private String message;

    @Override
    protected ModelAndView handleRequestInternal(HttpServletRequest request,
                                                 HttpServletResponse response) throws Exception {
        log.info("************* Message From Controller ************ " + message);
        ModelAndView model = new ModelAndView("HelloWorldPage");
        model.addObject("msg", "hello world from " + message);

        return model;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}